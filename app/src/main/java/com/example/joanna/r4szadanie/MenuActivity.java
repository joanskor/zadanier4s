package com.example.joanna.r4szadanie;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.joanna.r4szadanie.database.ProxyDatabase;
import com.example.joanna.r4szadanie.dto.CityLocalizationDTO;
import com.example.joanna.r4szadanie.dto.CityWebService;
import com.example.joanna.r4szadanie.model.City;
import com.example.joanna.r4szadanie.model.CityListAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_menu)
public class MenuActivity extends AppCompatActivity {

    private static final String TAG = MenuActivity_.class.getName();

    RestAdapter restAdapter;
    CityWebService cityWebService;

    @Bean
    ProxyDatabase database;

    @Bean
    CityListAdapter adapter;

    @ViewById(R.id.menu_toolbar)
    Toolbar menuToolbar;

    @ViewById(R.id.city_list_view)
    ListView cityListView;

    @ViewById(R.id.city_edit_text)
    EditText cityEditText;

    @ItemClick
    void cityListViewItemClicked(final City city){

        final Context context = this;

        try {
            cityWebService.getData(city.getName().toString(), new Callback<CityLocalizationDTO>() {
                @Override
                public void success(CityLocalizationDTO cityLocalizationDTO, Response response) {
                    Log.d(TAG, cityLocalizationDTO.status);

                    if (cityLocalizationDTO.status.equals("OK") && cityLocalizationDTO.getNation().equals("Poland")) {

                        city.setName(cityLocalizationDTO.getName());
                        city.setCommunity(cityLocalizationDTO.getCommunity());
                        city.setCounty(cityLocalizationDTO.getCounty());
                        city.setVoivodeship(cityLocalizationDTO.getVoivodeship());
                        city.setNation(cityLocalizationDTO.getNation());

                        ShowCityActivity_.intent(context).extra("message", city).start();

                    } else {
                        Toast.makeText(context, "Nie znaleziono miasta " + city.getName().toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, error.getLocalizedMessage());
                }
            });
        } catch (Exception e){
            Log.d(TAG, e.toString());
        }
    }

    @Click(R.id.add_city_button)
    void addCityButtonClicked(){
        String cityName = cityEditText.getText().toString();

        if (!cityName.matches("")){
            database.save(new City(cityName));
            cityEditText.setText("");
            recreate();
        }
    }

    @AfterViews
    void init(){
        cityListView.setAdapter(adapter);
        setSupportActionBar(menuToolbar);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://maps.googleapis.com/")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        cityWebService = restAdapter.create(CityWebService.class);
    }

}
