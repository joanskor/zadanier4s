package com.example.joanna.r4szadanie.dto;


import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface CityWebService {

    @GET("/maps/api/geocode/json")
    void getData(@Query("address") String name, Callback<CityLocalizationDTO> callback);

}
