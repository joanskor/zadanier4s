package com.example.joanna.r4szadanie.database;

import android.util.Log;

import com.example.joanna.r4szadanie.model.City;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

@EBean
public class ProxyDatabase implements DataBase {

    private static final String TAG = ProxyDatabase.class.getName();

    @Bean
    SugarOrmDatabase sugarOrmDatabase;

    @Override
    public void save(City city) {
        Log.d(TAG, city.getName() + " saved");
        sugarOrmDatabase.save(city);
    }

    @Override
    public void save(List<City> cityList) {
        sugarOrmDatabase.save(cityList);
    }

    @Override
    public List<City> getCities() {
        return sugarOrmDatabase.getCities();
    }

    @Override
    public City getCityById(long id) {
        return sugarOrmDatabase.getCityById(id);
    }

    @Override
    public List<City> getCitiesByName(String name) {
        return sugarOrmDatabase.getCitiesByName(name);
    }

    @Override
    public void deleteCity(long id) {
        sugarOrmDatabase.deleteCity(id);
        Log.d(TAG, getCityById(id) + " deleted");
    }

    @Override
    public void deleteAll() {
        sugarOrmDatabase.deleteAll();
        Log.d(TAG, getSize() + " elements deleted");
    }

    @Override
    public int getSize() {
        return sugarOrmDatabase.getSize();
    }
}
