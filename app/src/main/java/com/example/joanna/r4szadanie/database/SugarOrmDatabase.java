package com.example.joanna.r4szadanie.database;

import com.example.joanna.r4szadanie.model.City;

import org.androidannotations.annotations.EBean;

import java.util.List;

@EBean
public class SugarOrmDatabase implements DataBase {

    @Override
    public void save(City city) {

        city.save();
    }

    @Override
    public void save(List<City> cityList) {

        for (City city : cityList){
            city.save();
        }
    }

    @Override
    public List<City> getCities() {
        return City.listAll(City.class);
    }

    @Override
    public City getCityById(long id) {
        return City.findById(City.class, id);
    }

    @Override
    public List<City> getCitiesByName(String name) {
        return City.find(City.class, "name = ?", name);
    }

    @Override
    public void deleteCity(long id) {
        getCityById(id).delete();
    }

    @Override
    public void deleteAll() {
        City.deleteAll(City.class);
    }

    @Override
    public int getSize() {
        return getCities().size();
    }
}
