package com.example.joanna.r4szadanie.dto;

import java.util.List;

public class CityLocalizationDTO {

    public List<Result> results;
    public String status;

    public CityLocalizationDTO(List<Result> results, String status){
        this.status = status;
        this.results = results;
    }

    public String getName(){ return results.get(0).getAddressComponents().get(0).getLongName(); }

    public String getCommunity(){
        String community = results.get(0).getAddressComponents().get(1).getLongName();

        if (community.contains("Gmina")) {
            community = community.replace("Gmina"," ");
        }
        else if (community.contains("gmina")){
            community = community.replace("gmina"," ");
        }
        else if (community.contains("community")){
            community = community.replace("community"," ");
        }
        else if (community.contains("Community")) {
            community = community.replace("Community"," ");
        }
        return community;
    }

    public String getCounty(){
        String county = results.get(0).getAddressComponents().get(2).getLongName();

        if (county.contains("Powiat")) {
            county = county.replace("Powiat"," ");
        }
        else if (county.contains("powiat")){
            county = county.replace("powiat"," ");
        }
        else if (county.contains("county")){
            county = county.replace("county"," ");
        }
        else if (county.contains("County")) {
            county = county.replace("County"," ");
        }

        return county;
    }
    public String getVoivodeship(){
        String voivodeship = results.get(0).getAddressComponents().get(3).getLongName();

        if (voivodeship.contains("Województwo")) {
            voivodeship = voivodeship.replace("Województwo","");
        }
        else if (voivodeship.contains("województwo")){
            voivodeship = voivodeship.replace("województwo","");
        }
        else if (voivodeship.contains("voivodeship")){
            voivodeship = voivodeship.replace("voivodeship","");
        }
        else if (voivodeship.contains("Voivodeship")) {
            voivodeship = voivodeship.replace("Voivodeship"," ");
        }

        return voivodeship;
    }

    public String getNation(){
        for (Address address : results.get(0).getAddressComponents()){
            if (address.getTypes()[0].equals("country")){
                return address.long_name;
            }
        }
        return null;
    }

    class Result {

        List<Address> address_components;
        String formatted_address;
        Geometry geometry;
        String place_id;
        String[] types;

        public Result(List<Address> address_components, String formatted_address, Geometry geometry, String place_id, String[] types){
            this.address_components = address_components;
            this.formatted_address = formatted_address;
            this.geometry = geometry;
            this.place_id = place_id;
            this.types = types;
        }

        public List<Address> getAddressComponents(){ return address_components; }
        public String getFormattedAddress(){ return formatted_address; }
        public Geometry getGeometry(){ return geometry; }
        public String getPlaceId() { return place_id; }
        public String[] getTypes(){ return types; }
    }

    class Address {

        String long_name;
        String short_name;
        String[] types;

        public Address(String long_name, String short_name, String[] types){
            this.long_name = long_name;
            this.short_name = short_name;
            this.types = types;
        }

        public String getLongName(){ return long_name; }
        public String getShortName(){ return short_name; }
        public String[] getTypes(){ return types; }
    }

    class Geometry {

        Location location;
        String location_type;
        Viewport viewport;

        public Geometry(Location location, String location_type, Viewport viewport){
            this.location = location;
            this.location_type = location_type;
            this.viewport = viewport;
        }

        public Location getLocation(){ return location; }
        public String getLocationType(){ return location_type; }
        public Viewport getViewport(){ return viewport; }
    }

    class Viewport {

        Location northeast;
        Location southwest;

        public Viewport(Location northeast, Location southwest){
            this.northeast = northeast;
            this.southwest = southwest;
        }

        public Location getNortheast(){ return northeast; }
        public Location getSouthwest(){ return southwest; }
    }

    class Location {

        Float lat;
        Float lng;

        public Location(Float lat, Float lng){
            this.lat = lat;
            this.lng = lng;
        }

        public Float getLat(){ return lat; }
        public Float getLng(){ return lng; }
    }

}
