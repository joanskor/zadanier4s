package com.example.joanna.r4szadanie;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.joanna.r4szadanie.model.City;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_city)
@OptionsMenu(R.menu.menu_city)
public class ShowCityActivity extends AppCompatActivity {

    @Extra
    City message;

    @ViewById(R.id.name_city)
    TextView name;

    @ViewById(R.id.community_city)
    TextView community;

    @ViewById(R.id.county_city)
    TextView county;

    @ViewById(R.id.voivodeship_city)
    TextView voivodeship;

    @ViewById(R.id.nation_city)
    TextView nation;

    @ViewById(R.id.city_toolbar)
    Toolbar cityToolbar;

    @OptionsItem(R.id.action_back)
    void actionBackChoosen(){
        finish();
    }

    @Click(R.id.city_toolbar)
    void cityToolbarClicked(){
        finish();
    }

    @AfterViews
    public void init(){
        name.setText("Miejscowość: " + message.getName());
        community.setText("Gmina: " + message.getCommunity());
        county.setText("Powiat: " + message.getCounty());
        voivodeship.setText("Województwo: " + message.getVoivodeship());
        nation.setText(message.getNation());
        setSupportActionBar(cityToolbar);
        setTitle("Wróć");
    }

}
