package com.example.joanna.r4szadanie.model;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.joanna.r4szadanie.database.ProxyDatabase;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

@EBean
public class CityListAdapter extends BaseAdapter {

    private List<City> cityList = City.listAll(City.class);

    @RootContext
    Context context;

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public City getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CityItemView cityItemView;
        if (convertView == null) {
            cityItemView = CityItemView_.build(context);
        } else {
            cityItemView = (CityItemView) convertView;
        }

        cityItemView.bind(getItem(position));

        if (position%2 == 0){
            cityItemView.setBackgroundColor(Color.parseColor("#C5E1A5"));
        }

        return cityItemView;
    }
}
