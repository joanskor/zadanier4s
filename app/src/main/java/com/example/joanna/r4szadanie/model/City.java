package com.example.joanna.r4szadanie.model;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import java.io.Serializable;

@Table
public class City extends SugarRecord implements Serializable {

    private Long id;

    private String name;
    private String community;
    private String county;
    private String voivodeship;
    private String nation;

    public City(){}

    public City(String name){
        this.name = name;
    }

    public Long getId(){ return this.id; }
    public String getName(){ return this.name; }
    public String getCommunity(){ return this.community; }
    public String getCounty(){ return this.county; }
    public String getVoivodeship(){ return this.voivodeship; }
    public String getNation(){ return this.nation; }

    public void setName(String name){ this.name = name; }
    public void setCommunity(String community){ this.community = community; }
    public void setCounty(String county){ this.county = county; }
    public void setVoivodeship(String voivodeship){ this.voivodeship = voivodeship; }
    public void setNation(String nation){ this.nation = nation; }
}
