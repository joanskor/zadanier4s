package com.example.joanna.r4szadanie.model;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.joanna.r4szadanie.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.city_item)
public class CityItemView extends LinearLayout {

    private static final String TAG = CityItemView.class.getName();

    @ViewById(R.id.name_city_item)
    TextView nameCityItem;

    public CityItemView(Context context){
        super(context);
    }

    public void bind(City city){
        nameCityItem.setText(city.getName());
    }
}
