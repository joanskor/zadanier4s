package com.example.joanna.r4szadanie.database;

import com.example.joanna.r4szadanie.model.City;

import java.util.List;

public interface DataBase {

    void save(City city);

    void save(List<City> cityList);

    List<City> getCities();

    City getCityById(long id);

    List<City> getCitiesByName(String name);

    void deleteCity(long id);

    void deleteAll();

    int getSize();
}
